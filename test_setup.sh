#!/bin/bash

test_root=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export PATH=$PATH:$test_root/test:$test_root/testfw:$test_root/tests
source can_utils.sh
source test_utils.sh

export TARGET_USER=root
export TARGET_PASS=root

