#!/bin/bash
#===============================================================================
#
#          FILE:  launch_several.sh
# 
#         USAGE:  ./launch_several.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/10/2018 11:49:08 PM PST
#      REVISION:  ---
#===============================================================================

delay=1
for (( can_gen_sessions=0; can_gen_sessions<10; can_gen_sessions+=1 )); do
	echo -e "can_gen_sessions: $can_gen_sessions, delay: $delay"
		launcher.sh	--test=find_koops.sh \
				--args=$delay,$can_gen_sessions,8,1800 \
				--output=/home/cadler/byton/projects/ndrc/test/$delay'_'$can_gen_sessions
done

