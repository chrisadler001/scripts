#!/bin/bash
#===============================================================================
#
#          FILE:  parse_koops_results.sh
# 
#         USAGE:  ./parse_koops_results.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/12/2018 08:14:57 AM PST
#      REVISION:  ---
#===============================================================================

OUTPUT_STRING=""

function process_ifconfig ()
{
	local int rx tx
	local ifconfig_path=$1
	while read -u 10 line; do
		echo -n $line | grep -q "can" && int=$(echo $line | cut -d':' -f1)
		if [[ ! -z ${int+x} ]]; then
			echo -n $line | grep -q "RX packets" && \
				rx=$(echo $line | cut -d' ' -f3,5 --output-delimiter=",")	
			echo -n $line | grep -q "TX packets" && \
				tx=$(echo $line | cut -d' ' -f3,5 --output-delimiter=",")
		fi
	
		if [ ! -z ${int+x} ] && [ ! -z ${rx+x} ] && [ ! -z ${tx+x} ]; then
			OUTPUT_STRING+=$int','$rx','$tx$' '
			unset int rx tx
		fi	
	done 10<$ifconfig_path
}    # ----------  end of function process_ifconfig  ----------


function process_result ()
{
	results_path=$1
	local result=pass 
	local ms_gap=$(grep KOOPS_PARAM_FRAME_DELAY $results_path/log.txt | cut -d'=' -f2)	
	local mul_bus=$(grep KOOPS_PARAM_NUM_BUSSES $results_path/log.txt | cut -d'=' -f2)	
	local num_bytes=$(grep KOOPS_PARAM_NUM_BYTES $results_path/log.txt | cut -d'=' -f2)	
	local test_time=$(grep KOOPS_PARAM_TIMEOUT_SECONDS $results_path/log.txt | cut -d'=' -f2)	
	local start_time=$(grep StartTime $results_path/log.txt | cut -d':' -f2)	
	local stop_time=$(grep StopTime $results_path/log.txt | cut -d':' -f2)
	local run_time=$(( $stop_time-$start_time ))	
	if [[ -e $results_path/fail ]]; then
		result=fail
	fi
	OUTPUT_STRING=$results_path','$result','$ms_gap','$mul_bus','$num_bytes','$test_time','$run_time
}    # ----------  end of function process_results  ----------



function process_test_csv ()
{
	process_result $1
	local prefix=$OUTPUT_STRING
	OUTPUT_STRING=""	
	process_ifconfig $1/ifconfig.txt
	for i in $OUTPUT_STRING; do
		echo $prefix','$i | tee -a $output_csv
	done
}    # ----------  end of function process_test  ----------


function print_header_csv ()
{
	echo -e "path, status, gap_delay, sessions_per_bus, payload_length, test_time, run_time, bus_name, rx_packets, rx_bytes, tx_packets, tx_bytes" | tee $output_csv
}    # ----------  end of function print_header_csv  ----------

test_root_results=$(readlink -f $1)
output_csv=$2



# process all directories in the root as test results
print_header_csv
for test_directory in $(ls -da $test_root_results/*); do
	echo $test_directory
	process_test_csv $test_directory
done


