#!/bin/bash
#===============================================================================
#
#          FILE:  find_koops.sh
# 
#         USAGE:  ./find_koops.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/09/2018 05:05:44 PM PST
#      REVISION:  ---
#===============================================================================

KOOPS_PARAM_FRAME_DELAY=${KOOPS_PARAM_FRAME_DELAY:-100}
KOOPS_PARAM_NUM_BUSSES=${KOOPS_PARAM_NUM_BUSSES:-30}
KOOPS_PARAM_NUM_BYTES=${KOOPS_PARAM_NUM_BYTES:-8}
KOOPS_PARAM_TIMEOUT_SECONDS=${KOOPS_PARAM_TIMEOUT_SECONDS:-\-}

local pids=()

function initialize ()
{
	KOOPS_PARAM_FRAME_DELAY=$1
	KOOPS_PARAM_NUM_BUSSES=$2
	KOOPS_PARAM_NUM_BYTES=$3
	KOOPS_PARAM_TIMEOUT_SECONDS=$4

	echo -e "Test parameters:\n"	
	echo -e "KOOPS_PARAM_FRAME_DELAY=$1\n"
	echo -e "KOOPS_PARAM_NUM_BUSSES=$2\n"
	echo -e "KOOPS_PARAM_NUM_BYTES=$3\n"
	echo -e "KOOPS_PARAM_TIMEOUT_SECONDS=$4\n"

}    # ----------  end of function initialize  ----------

function setup ()
{
	reboot_target
	#toggle-can 0 3
}    # ----------  end of function setup  ----------


function run ()
{
	if [[ "$KOOPS_PARAM_TIMEOUT_SECONDS" != "-" ]]; then
		local timeout=$((`date +'%s'`+$KOOPS_PARAM_TIMEOUT_SECONDS))
	fi

	# keep track of the pids to kill them later
	local found_koops=0
	
	# spawn canbus loaders
	for i in $(seq 1 $KOOPS_PARAM_NUM_BUSSES); do
		canblast.sh - $KOOPS_PARAM_FRAME_DELAY&
		#canblast.sh - $KOOPS_PARAM_FRAME_DELAY
		pids+="$! "
	done
	
	while [[ 1 ]]; do 
		sshpass -p "$TARGET_PASS" scp $TARGET_USER@$TARGET_IP:/var/log/syslog syslog
		# if we find this signature, then we've koops'ed	
		if grep -rin "end trace" syslog; then
			return 1;
		fi

		if [[ "$KOOPS_PARAM_TIMEOUT_SECONDS" != "-" ]]; then
			if [[ $timeout -lt $((`date +'%s'`)) ]]; then
				return 0;
			fi
		fi

		sleep 5	
	done
}	

function cleanup ()
{
	echo -e "Cleanup....\n"
	sshpass -p "$TARGET_PASS" ssh $TARGET_USER@$TARGET_IP ip -d link show type can > can_stats.txt
	sshpass -p "$TARGET_PASS" ssh $TARGET_USER@$TARGET_IP ifconfig > ifconfig.txt
	
	for i in $pids; do
		kill $i
	done
}    # ----------  end of function cleanup  ----------
