#!/bin/bash
#===============================================================================
#
#          FILE:  canblast.sh
# 
#         USAGE:  ./canblast.sh 
# 
#   DESCRIPTION: Generates a programmatic shit ton of CAN traffic across busses. 
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/09/2018 11:24:51 AM PST
#      REVISION:  ---
#===============================================================================

pids=()

ifs=$1
ms=$2
bytes=$3

#===  FUNCTION  ================================================================
#          NAME:  
#   DESCRIPTION:  
#    PARAMETERS:  
#       RETURNS:  
#===============================================================================
findcans(){
	echo $(ip link show type can | cut -d':' -s -f2)
}

function trap_ctlc ()
{
	echo -e "caught the ctlc\n"
	for pid in $pids; do
		if ps --no-headers $pid; then
			echo -e "killing... $(ps --no-headers $pid)\n"
			kill -9 $pid
		fi	
	done	
	exit
}    # ----------  end of function trap_ctlc  ----------

trap trap_ctlc INT SIGINT SIGTERM

# - means use all interfaces present for command
if [[ "$ifs" = "-" ]]; then
	ifs=$(findcans)
else
	ifs=$(echo $ifs | sed 's/,/ /g')
fi
echo -e "Launching cangen sessions...\n"
for i in $ifs; do
	cmd="cangen -g $ms $i"
	$cmd&
	ps $! > /dev/null && pids+="$! "	
	echo -e "[$!] $cmd\n"	
done

echo -e "Entering infinite loop...\n"
while true; do
	sleep 1
done
