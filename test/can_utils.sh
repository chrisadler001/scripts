#!/bin/bash
#===============================================================================
#
#          FILE:  can_utils.sh
# 
#         USAGE:  source can_utils.sh 
# 
#   DESCRIPTION:  Some functions to work with can busses.
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Chris Adler (chris.adler@byton.com) 
#       COMPANY: 	Byton 
#       VERSION:  1.0
#       CREATED:  02/13/2018 08:21:11 AM PST
#      REVISION:  ---
#===============================================================================
source test_utils.sh 

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  target_list_can_busses
#   DESCRIPTION:  Lists all CAN busses on remote target.
#    PARAMETERS:  None 
#       RETURNS: 	can busses of remote target, one per line 
#-------------------------------------------------------------------------------
target_list_can_busses ()
{
	ssh_command "ip link show type can" | cut -d':' -f2 -s
}    # ----------  end of function target_list_can_busses  ----------


#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  target_can_bus_exist
#   DESCRIPTION:  Checks to make that CAN bus ($1) exists. 
#    PARAMETERS:  $1-bus name 
#       RETURNS:  0-bus exists; 1-bus does not exist 
#-------------------------------------------------------------------------------
target_can_bus_exist ()
{
	target_list_can_busses | grep -q -w $1	
}	# ----------  end of function target_can_bus_exist  ----------

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  target_can_bus_receive_count 
#   DESCRIPTION:  Returns the number of received packets on CAN bus. 
#    PARAMETERS:  $1-bus name 
#       RETURNS:  0-success, with count; 1-failure 
#-------------------------------------------------------------------------------
target_can_bus_receive_count ()
{
	# Look for the bus on the target first...
	target_can_bus_exist "$1" || 
	{ echo "$1 not found on target." 1>&2; return 1; }
	
	# ...and then report the count.
	ssh_command "netstat -i" | grep $1 | awk '{print $3}'
}	# ----------  end of function target_can_bus_receive_count  ----------

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  target_is_can_bus_receiving
#   DESCRIPTION:	Polls CAN bus to see it receiving data. 
#    PARAMETERS: 	$1-bus name, $2-number of seconds to timeout 
#       RETURNS:  0-success; 1-failure 
#-------------------------------------------------------------------------------
target_is_can_bus_receiving()
{
	local timeout=${2:-10} can_bus=$1
	
	while [[ $timeout -ge 0 ]]; do
		target_count=$(can_bus_receive_count "$can_bus")
		timeout=$(( $timeout-1 ))

		# one-shot to initialize the count on the first pass
		validate_variable last_count || local last_count=$target_count
		if [[ $target_count -gt $last_count ]]; then
			return 0
		fi
		sleep 1
	done
	return 1
}	# ----------  end of function is_can_bus_receiving----------


#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_find_target_bus_from
#   DESCRIPTION:  Discovers what target CAN bus is connected to a host CAN bus
#    PARAMETERS:  $1-host CAN bus  
#       RETURNS:  0-bus found, with name; 1-bus not found  
#-------------------------------------------------------------------------------
host_find_target_bus_from ()
{
	local local_bus=$1 last_bus current_bus
	local target_busses=$(target_list_can_busses)
	local cmd="cangen -n 5 -g 10 $local_bus"

	# Sweep through the target busses and look for incrementing
	for bus in $target_busses; do
		last=$(target_can_bus_receive_count $bus)
		$cmd || { echo "$cmd failed!" 1>&2; return 1; }
		current=$(target_can_bus_receive_count $bus)
		[[ $current -gt $last ]] && { echo $bus; return 0; }
	done

	# Nothing found... report an error
	return 1;
}	# ----------  end of function host_find_target_bus_from  ----------

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_list_can_busses
#   DESCRIPTION:  Lists CAN busses on host, one per line.
#    PARAMETERS:  None 
#       RETURNS: 	0-success, with bus names; 1- failure 
#-------------------------------------------------------------------------------
host_list_can_busses ()
{
	ip link show type can | cut -d':' -f2 -s
}	# ----------  end of function host_list_can_busses  ----------

#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_map_can_to_target
#   DESCRIPTION:  Remaps host CAN bus names to target CAN bus names. 
#    PARAMETERS: 	None. 
#       RETURNS: 	None.  
#-------------------------------------------------------------------------------
host_map_can_to_target ()
{
	local host_busses=$(host_list_can_busses)
	printf "Host has:\n$host_busses\n\n"
	
	local num=0
	for host_bus in $host_busses; do
		sudo ifconfig $host_bus down
		sudo ip link set $host_bus name tempcan$num
		sudo ifconfig tempcan$num up 
		num=$(($num+1))
	done

	host_busses=$(host_list_can_busses)
	printf "Host now has:\n$host_busses\n\n"

	printf "Cross checking busses and fixing busses...\n"	
	for host_bus in $host_busses; do
		# make sure that the bus is up
		sudo ifconfig $host_bus up
		printf "Host [$host_bus] -> Target "	
		target_bus=$(host_find_target_bus_from $host_bus)
		printf "[$target_bus]\n"
		sudo ifconfig $host_bus down
		sudo ip link set $host_bus name $target_bus
		sudo ifconfig $target_bus up
	done
	printf "Fixing complete...\n\n"	
	host_busses=$(host_list_can_busses)
	printf "Host has:\n$host_busses\n"
}	# ----------  end of function host_map_can_to_target  ----------


#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_can_bus_exist
#   DESCRIPTION:  Checks if CAN bus name ($1) exists on host.
#    PARAMETERS:  $1-CAN bus name 
#       RETURNS: 	0-CAN bus exists; 1-CAN bus does not exist 
#-------------------------------------------------------------------------------
host_can_bus_exist ()
{
	host_list_can_busses | grep -q -w $1	
}	# ----------  end of function host_can_bus_exist  ----------


#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_can_bus_exist
#   DESCRIPTION:  Resets CAN bus name ($1) on host. 
#    PARAMETERS: 	$1-CAN bus name, $2-bitrate 
#       RETURNS: 	0-CAN bus reset; 1-failure	 
#-------------------------------------------------------------------------------
host_reset_can_bus ()
{
	local can=$1 bitrate=${2:-500000}
	[[ -z $can ]] && { echo "No bus specified." 1>&2; return 1; }

	# Look for the bus on the host first...
	host_can_bus_exist $can || 
	{ echo "$can not found on host." 1>&2; return 1; }

	# ...then reset the bus
	{ sudo ip link set "$can" down && 
		sudo ip link set $can up type can bitrate $bitrate; } ||
		{ echo "Could not reset $can." 1>&2; return 1; }
}	# ----------  end of function host_reset_can_bus  ----------


#---  FUNCTION  ----------------------------------------------------------------
#          NAME:  host_reset_all_can_busses
#   DESCRIPTION:  Resets all CAN busses discovered on host.
#    PARAMETERS:  $1-bitrate 
#       RETURNS: 	0-All CAN busses reset; 1-failure 
#-------------------------------------------------------------------------------
host_reset_all_can_busses ()
{
	local bitrate=${1:-500000}
	for can_bus in $(host_list_can_busses); do
		host_reset_can_bus $can_bus $bitrate || return 1
	done
}	# ----------  end of function host_reset_all_can_busses  ----------
