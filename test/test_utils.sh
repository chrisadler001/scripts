#!/bin/bash
#===============================================================================
#
#          FILE:  test_utils.sh
# 
#         USAGE:  source test_utils.sh 
# 
#   DESCRIPTION:  A place to get convenient test utilities (functions).
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Chris Adler (chris.adler@byton.com), 
#       COMPANY: 	Byton 
#       VERSION:  1.0
#       CREATED:  02/10/2018 08:39:00 AM PST
#      REVISION:  ---
#===============================================================================


function validate_variable ()
{
	[ "${!1}" = "" ] && 
		{ echo "$1 is unset." 1>&2; return 1; }
	return 0;
}    # ----------  end of function validate_variable  ----------

function reboot_target ()
{
	# make sure that the right target variables have been setup
	{ validate_variable TARGET_IP &&
	  validate_variable TARGET_USER &&	
		validate_variable TARGET_PASS; } || return 1;
	local retries=5
	
	echo -e "Rebooting $TARGET_IP...\n"
	sshpass -p "$TARGET_PASS" ssh -o TCPKeepAlive=yes -o ServerAliveInterval=5 $TARGET_USER@$TARGET_IP reboot -d 5 -f
	# now we block until we can connect again...
	
	while [[ $retries > 0 ]]; do
		sleep 1
		echo -e "Attempting reconnect to $TARGET_IP ($retries)...\n"	
		if sshpass -p "$TARGET_PASS" ssh -af $TARGET_USER@$TARGET_IP ls; then
			break
		fi
		retries=$(($retries-1))
	done

	return $(($retries==0))
}    # ----------  end of function reboot_target  ----------


function ssh_command ()
{
	# make sure that the right target variables have been setup
	{ validate_variable TARGET_IP && 
	validate_variable TARGET_USER && 
	validate_variable TARGET_PASS; }  &&
	sshpass -p "$TARGET_PASS" ssh $TARGET_USER@$TARGET_IP "$1" 
}    # ----------  end of function ssh_command  ----------


