#!/bin/bash
#===============================================================================
#
#          FILE:  test_parse.sh
# 
#         USAGE:  ./test_parse.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/10/2018 08:18:00 PM PST
#      REVISION:  ---
#===============================================================================

PARSED_OPTIONS=$(getopt -n "$0" -o t:o:a: --long "test:,output:,args:" -- "$@")

eval set -- "$PARSED_OPTIONS"

while true; do
	case "$1" in
		 
		-t|--test)
			echo "test"
			if [ -n "$2" ]; then
				echo $2
			fi
			shift 2;;

		-o|--output)
			echo "output"
			if [ -n "$2" ]; then
				echo $2
			fi
			shift 2;;
	
		-a|--args)
			echo "args"
			if [ -n "$2" ]; then
				echo $2
			fi
			shift 2;;

		--)
			shift
			break;;
	esac
done
