#!/bin/bash
#===============================================================================
#
#          FILE:  launcher.sh
# 
#         USAGE:  ./launcher.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   (), 
#       COMPANY:  
#       VERSION:  1.0
#       CREATED:  02/10/2018 04:24:24 PM PST
#      REVISION:  ---
#===============================================================================

function launchertrap()
{
	echo -e "launcher caught the ctlc\n"
	cleanup
	exit
}    # ----------  end of function trap_ctlc  ----------

trap launchertrap INT SIGINT SIGTERM

function do_test ()
{

	echo -e "StartTime: `date +'%s'`\n"	
	# change context to be in the output directory
	if ! cd $OUTPUT_DIR; then
		echo -e "Could not cd to $OUTPUT_DIR. Exiting\n"
		exit 1
	fi

	# source the test script
	source $TEST_PATH
	
	# initialize the test
	if ! initialize $(echo $TEST_ARGS | sed 's/,/ /g'); then
		echo -e "initialize failed. Exiting.\n"
		exit 1
	fi
	
	# perform setup operations
	if ! setup; then
		echo -e "setup failed. Exiting.\n"
		exit 1
	fi

	# put a failed file in the directory if the test fails	
	run || touch $OUTPUT_DIR/fail 
	
	cleanup
	
	echo -e "StopTime: `date +'%s'`\n"	
}    # ----------  end of function do_test  ----------


PARSED_OPTIONS=$(getopt -n "$0" -o t:o:a: --long "test:,output:,args:" -- "$@")

eval set -- "$PARSED_OPTIONS"

while true; do
	case "$1" in
		 
		-t|--test)
			if [ -n "$2" ]; then
				TEST_PATH=$2	
			fi
			shift 2;;

		-o|--output)
			if [ -n "$2" ]; then
				OUTPUT_DIR=$2	
			fi
			shift 2;;
	
		-a|--args)
			if [ -n "$2" ]; then
				TEST_ARGS=$2	
			fi
			shift 2;;

		--)
			shift
			break;;
	esac
done

# make the test directory to store results in
if ! mkdir -p $OUTPUT_DIR; then
	echo -e "Could not make $OUTPUT_DIR. Exiting.\n"
	exit 1
fi

do_test > $OUTPUT_DIR/log.txt

